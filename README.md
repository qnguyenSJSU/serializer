# How to use Serializer API

Table of Contents

1. About the project
    - Bulit With
2. Getting Started
    - Prerequisites
    - Installation
3. Usage
4. Contacts

# About the project

The purpose of this project is making a serializer API helping user who want to write networking programs.

It will provides necessary functions to reduce complexity of networking programming.

### Bulit With

- Python3


# Getting Started

- Prerequisties

  - Python3
    - On Mac OS
    ```
    brew install python3
    ```
    - On Window
        - Download Python3.6 or later version at Microsoft Store
  - If you download python3 properly, then pip3 will be installed automatically with Python3

  - You can check your Python version by excuting Python3.

  ![Image of Python3 version check](md1.png)

- Installation
    - Clone the repo
    - Copy and Paste serializer.py file to your py file

# Usage
  - ### Sample useage
  ![Image of Python3 version check](md3.png)
  ![Image of Python3 version check](md4.png)

  - ### About format()
    The format function accepts an list of dictionaries. Each dictionary should contain a fieldname , a type, and any additional parameters that you need to pass into the encoding/decoding function. Here is an example of a valid input to format:
    ```python
    my_format = [
      {'name': 'source_ip', 'type': 'ipv4', 'value': src_ipv4},
      {'name': 'source_mac', 'type': 'mac', 'value': src_mac},
      {'name': 'protocol_number', 'type': 'int', 'length': 2 }
    ]
    Packer().format(my_format).feed(...)
    ```
    
    ### User can choose an one of following types:
    ### int, pstring, cstring, ipv4, ipv6, mac
    ![exampleChart](md2.png)

