import socket
import base64
import codecs
from random import randrange

class Unpacker(object):
    def __init__(self):
        self.form = []

    def format(self, format):
        for dic in format:
            if ('name' not in dic or 'type' not in dic):
                raise KeyError("Missing key 'name' or 'type' in format element {}.".format(dic) )

        self.form = format
        return self

    def feed(self, name, data):
        found = False
        for dic in self.form:
            if(dic['name'] is name):
                decoded_val = EncoderDecoder().decode(data=data, **dic)
                dic['output'] = decoded_val
                found = True
                break
        if not found:
            raise KeyError('Field name {} does not exist.', name)
        return self

    def to_dict(self):
        output_dict = {}
        for dic in self.form:
            if 'output' in dic:
                output_dict[dic['name']] = dic['output']
            else:
                output_dict[dic['name']] = None
        return output_dict


# Unpacker()
#     .format([
#         {'name': 'opcode', 'type': 'insdasdast', 'length': 2},
#         {'name': 'err_code', 'type': 'int', length': 2},
#         {'name': 'msg', 'type': 'cstring'},
#     ])
#      .unpack_all(b'\x00\x04\x00\x05errormessage\0')
# Output
# {'opcode': 4, 'err_code': 5, 'msg': 'errormessage' },


    def unpack_all(self, data):
        start = 0
        end = 0
        for dic in self.form:
            # Find the start and end of the first piece of data
            end = start + self._calc_input_length(data[start:], dic)                
            byte_data = data[start:end]
            decoded_val = EncoderDecoder().decode(data=byte_data, **dic)
            # decode byte data
            dic['output'] = decoded_val
            start = end
        return self.to_dict()
    
    def _calc_input_length(self, data, dic):
        if dic['type'] == 'ipv4':
            return 4
        elif dic['type'] == 'ipv6':
           return 16
        elif dic['type'] == 'mac':
            return 6
        elif dic['type'] == 'int':
            return dic['length']
        elif dic['type'] == 'pstring':
            return EncoderDecoder().decode(data, 'int', length=2)
        elif dic['type'] == 'cstring':
            end =  data.find(b'\0')   
            # if not found then end is -1, handle error
            if end == -1:
                raise ValueError('Error parsing null-terminated string for field {}: No null character found.'.format(dic['name']))
            return end
        elif dic['type'] == 'raw':
            return len(data)
        else:
            raise KeyError('Invalid type {} for field {}'.format(dic['type'], dic['name']))



# Usecase for Packer
# serializer.packer()
#     .format([
#         {'opcode':{'type': 'int'},{'length': 2}},
#         {'err_code':{'type': 'int'},{'length': 2}},
#         {'msg', 'type': 'cstring'},
#     ])
#     .feed('opcode', TFTPServer.ERROR_OPCODE)
#     .feed('err_code', err_code)
#     .feed('msg', err_msg)
#     .to_bytes()


class Packer(object):
    def __init__(self):
        self.barr = b''
        self.form = []

    def format(self, format):
        for dic in format:
            if ('name' not in dic or 'type' not in dic):
                raise KeyError("Missing key 'name' or 'type' in format element {}.".format(dic) )

            if 'value' in dic:
                val = EncoderDecoder().encode(data=dic['value'], **dic)
                self.barr += val
        self.form = format
        return self

    def feed(self, name, data):
        self.barr = b''
        found = False
        for dic in self.form:
            if(dic['name'] == name):
                dic['value'] = data
                found = True
                break
        if not found:
            raise KeyError('Field name {} does not exist.', name)
        return self.format(self.form)

    def to_bytes(self):
        return self.barr


class EncoderDecoder(object):

    # encode() and decode() are interfaces other classes will use to interact with the EncoderDecoder
    # To use these functions in Packer/Unpacker, pass in the data, the type of data, and any additional arguments you might need

    # Some valid ways of calling the encoding function with additional arguments
    #   EncoderDecoder.encode(data=4, type='int', length=2)
    #   EncoderDecoder.encode(**{'data': 4, 'type': 'int', 'length': 2}) #using dictionary unwrapping
    #   EncoderDecoder.encode(data=4, type='int', length=2, foo='foo', bar='bar') #irrelevant arguments simply get ignored
    # Look up python kwargs (keyword arguments) for more info on how to use arguments of unknown names and quantities
    def __init__(self):
        self.format_elements = {
            'int': (self.encode_int, self.decode_int),
            'pstring': (self.encode_pstring, self.decode_pstring),
            'cstring': (self.encode_cstring, self.decode_cstring),
            'ipv4': (self.encode_ipv4, self.decode_ipv4),
            'ipv6': (self.encode_ipv6, self.decode_ipv6),
            'mac': (self.encode_mac, self.decode_mac),
            'raw': (self.pass_raw, self.pass_raw),
            # 'bool': (),
        }


    def encode(self, data, type, **kwargs):
        if type in self.format_elements:
            return self.format_elements[type][0](data, **kwargs)
        else:
            raise KeyError("Invalid format type {}.".format(type))

    def decode(self, data, type, **kwargs):
        if type in self.format_elements:
            return self.format_elements[type][1](data, **kwargs)
        else:
            raise KeyError("Invalid format type {}.".format(type))

    def encode_int(self, num, **kwargs):
        if 'length' not in kwargs:
            # throw an Exception if you don't have the argument you need
            raise KeyError("Encoding integer requires additional argument 'length'.")
        return num.to_bytes(kwargs['length'], byteorder='big')

    def decode_int(self, num, **kwargs):
        if 'length' not in kwargs:
            # throw an Exception if you don't have the argument you need
            raise KeyError("Decoding integer requires additional argument 'length'.")
        
        return int.from_bytes(num[:kwargs['length']], byteorder='big')

    # P-strings, or pascal strings, are string that are preceded by a single byte denoting their length
    # An example would be \x05Hello

    def encode_pstring(self, string, **kwargs):
        length = len(string).to_bytes(1, 'big')
        return length + string.encode()

    def decode_pstring(self, byte_string, **kwargs):
        length = byte_string[0] + 1
        return str(byte_string[1:length], 'utf-8')

    # C-strings are null terminated strings
    # An example would be Hello\0

    def encode_cstring(self, string, **kwargs):
        return string.encode() + b'\0'

    def decode_cstring(self, byte_string, **kwargs):
        return str(byte_string[:-1], 'utf-8')

    def encode_ipv4(self, ip, **kwargs):
        return socket.inet_aton(ip)

    def decode_ipv4(self, byte_strin, **kwargs):
        return socket.inet_ntoa(byte_string)

    def encode_ipv6(self, ip, **kwargs):
        return socket.inet_aton(ip)

    def decode_ipv6(self, byte_string, **kwargs):
        return socket.inet_ntoa(byte_string)

    def encode_mac(self, mac, **kwargs):
        return codecs.decode(mac.replace(':', '').replace('-', '').encode('ascii'), "hex")

    def decode_mac(self, mac_bytes, **kwargs):
        s = base64.b16encode(mac_bytes)
        return ':'.join([s[i:i+2].decode('ascii') for i in range(0, 12, 2)])

    def pass_raw(self, raw, **kwargs):
        return raw

##TODO: To be deleted after testing with Encode decode and before merging##


def do_test():
    # result = (Unpacker()
    # .format([
    #     {'name': 'opcode', 'type': 'int', 'length': 2},
    #     {'name': 'err_code', 'type': 'int', 'length': 2},
    #     {'name': 'msg', 'type': 'pstring'},
    # ])
    #  .unpack_all(b'\x00\x04\x00\x05\x05errormessage')
    #  )
    # print(result)

    result = (Packer().format([
        {'name': 'opcode', 'type': 'int', 'length': 2},
        {'name': 'err_code', 'type': 'int', 'length': 2},
        {'name': 'msg', 'type': 'cstring'},
      ])
        .feed('opcode', 5)
        .feed('err_code', 17)
        .feed('msg', "No such file specified")
        .to_bytes()
    )
    print(result) #b'\x00\x05\x00\x11No such file specified\x00'

    result = (
        Unpacker().format([
        {'name': 'opcode', 'type': 'int', 'length': 2},
        {'name': 'err_code', 'type': 'int', 'length': 2},
        {'name': 'msg', 'type': 'cstring'},
      ])
     .feed('opcode', b'\x00\x05\x04\x00')
     .feed('err_code', b'\x00\x11')
     .feed('msg', b'No such file specified\0')
     .to_dict()
      )
    print(result) #{'opcode': 5, 'err_code': 17, 'msg': 'No such file specified'}


if __name__ == '__main__':
    do_test()
