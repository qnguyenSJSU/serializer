from serializer import Packer
from serializer import Unpacker

ip_format = [
        {'name': 'total_length', 'type': 'int', 'length' : 2},
        {'name': 'id', 'type': 'int', 'length' : 2},
        {'name' :'flags_and_offset', 'type': 'raw', 'length': 2},
        {'name': 'time_to_live', 'type': 'int', 'length': 1},
        {'name': 'protocol', 'type': 'int', 'length': 1},
        {'name': 'checksum', 'type': 'raw', 'length': 2},
        {'name': 'sip', 'type': 'ipv4'},
        {'name': 'dip', 'type': 'ipv4'},
    ]
udph_format = [{'name':'source_port','type':'int','length':2},
    {'name':'destination_port','type':'int','length':2},
    {'name': 'Length','type':'int','length':2},
    {'name': 'checksum', 'type':'raw','length':2}]

def construct_packet(inter, dmac, sip, dip, bootp):
    # BOOTP Payload
    data = bootp.to_bytes()

    # IP header
    ip = construct_ip_header(sip, dip, len(data))

    # UDP header
    #the ports the programmer hardcoded for the udp packet. no idea where they come from
    source_port = 67  
    dest_port = 68
    udp = construct_udp_header(source_port, dest_port, len(data))
    #compute and fill in the checksum
    udp[6:8] = compute_udp_checksum2(ip, udp, data)

    # Ethernet Frame
    ethertype_ipv4 = b'\x08\x00'
    smac = get_source_mac()
    ether = construct_ethernet_frame(smac, dmac, ethertype_ipv4)

    packet = ether + ip + udp + data
    return packet

def construct_ip_header(sip, dip, data_length) :
    ip_header_length = 20
    udp_header_length = 8
    total_length = ip_header_length + udp_header_length + data_length
    datagram_id = randrange(0, 65535)
    time_to_live = 64
    protocol = 17  #17 specifies the udp protocol
    placeholder_checksum = b'\x00\x00'

    # ip = b''
    # #value of 2 nibble-sized fields at once. 4 is for the ip version, 5 is for the header's length when it contains no option
    # ip += b'\x45'    #index 0
    # #another 2 fields - differentiated service field (6 bits) and explicit congestion notification (2 bits) - they're just zeroes
    # ip += b'\x00'   # index 1
    # ip += total_length.to_bytes(2, 'big')  #index 2:4
    # ip += datagram_id.to_bytes(2, 'big')   #index 4:6
    # # Flags (3 bits) and fragment offset (13 bits). hard coded by programmer, no idea what they mean
    # ip += b'\x40\x00'  #index 6:8
    # ip += time_to_live.to_bytes(1, 'big') #index 9
    # ip += protocol.to_bytes(1, 'big') #index 10
    # ip += placeholder_checksum #index 10:12
    # #source ip
    # ip += inet_aton(sip) #index 12:16
    # #dest ip
    # ip += inet_aton(dip) #index 16:20
    # #compute checksum after filling everything out
    # ip[10:12] = compute_checksum(ip)
    # return ip

    ip = b''
    ip += b'\x45'    #index 0
    ip += b'\x00'   # index 1
    
    header_body = (Packer().format(ip_format)
    .feed('total_length', total_length)
    .feed('id', datagram_id)
    .feed('flags_and_offset', b'\x40\x00')
    .feed('time_to_live', time_to_live)
    .feed('protocol', protocol)
    .feed('checksum', placeholder_checksum)
    .feed('sip', sip)
    .feed('dip', dip)
    .to_bytes()
    )
    ip +=  header_body
    ip[10:12] = compute_checksum(ip)
    return ip



def compute_udp_checksum2(ip_header, udp_header, data):
    '''
    protocol_number = ip_header[10]
    sip = ip_header[12:16]
    dip = ip_header[16:20]
    udp_length = udp_header[4:6]
    '''
    ip_pack = (Unpacker().format(ip_format)
        .unpack_all(ip_header))
    protocol_number = ip_pack['protocol_number']
    sip = ip_pack['sip']
    dip = ip_pack['dip']
    udp_pack = (Unpacker().format(udp_header).unpack_all(udp_header))
    udp_length = ip_pack['Length']

    checksum_data = construct_udp_pseudo_header(sip, dip, protocol_number, udp_length)
    checksum_data += udp_header
    checksum_data += data
    # For the purpose of the checksum, if the data length is odd then a pad byte must be added
    checksum_data +=  b'\x00' if (len(data) % 2 == 1) else b''
    return compute_checksum(checksum_data)


#The pseudo header is only there to compute the udp checksum. it is composed of
#the source and destination ip from the IPv4 header, a zero byte, a 1-byte protocol number from the udp header, 
#and the length of the udp packet (the length of its data plus the length of its header, 8)
def construct_udp_pseudo_header(sip, dip, protocol_number, udp_length):
    '''
    pseudoUDP = sip                   # Source IP
    pseudoUDP += dip                   # DEST ip
    pseudoUDP += b'\x00'           #pad byte
    pseudoUDP += protocol_number
    pseudoUDP += udp_length
    '''
    pseudoudp_format = [{'name':'sip','type':'ipv4','value':sip},
    {'name':'dip','type':'ipv4', 'value':dip},
    {'name': 'pad_byte', 'type':'raw', 'length':1, 'value':b'\x00'},
    {'name': 'protocol_number', 'type':'int','length':1, 'value': protocol_number},
    {'name': 'Length','type':'int','length':2, 'value': udp_length}]
    pseudoUDP = (Packer().format(pseudoudp_format).to_bytes())
    return pseudoUDP

def construct_udp_header(source_port, dest_port, data_length):
    
    placeholder_checksum = b'\x00\x00'
    udp_length = data_length + 8
    '''
    udp = (source_port).to_bytes(2, 'big')  #[0:2]
    udp += (dest_port).to_bytes(2, 'big')   #[2:4]
    udp += (udp_length).to_bytes(2, 'big')   #[4:6]
    udp += placeholder_checksum   #[6:8]
    '''
    udp = (Packer().format(udph_format)
        .feed('source_port', source_port)
        .feed('destination_port', dest_port)
        .feed('Length', data_length)
        .feed('checksum', placeholder_checksum)
        .to_bytes()
        )
    return udp

# https://github.com/mdelatorre/checksum/blob/master/ichecksum.py
def compute_checksum(data):
    sum = 0
    for i in range(0,len(data),2):
        if i + 1 >= len(data):
            sum += data[i] & 0xFF
        else:
            w = ((data[i] << 8) & 0xFF00) + (data[i+1] & 0xFF)
            sum += w

    while (sum >> 16) > 0:
        sum = (sum & 0xFFFF) + (sum >> 16)

    sum = ~sum
    sum = sum & 0xFFFF

    return (sum).to_bytes(2, 'big') 

def get_source_mac():
    try:
        mac = open('/sys/class/net/'+inter+'/address').readline()
    except:
        print("Failed to get mac adress for ", inter)
    return mac[0:17]

def construct_ethernet_frame(smac, dmac, ethertype):
    '''
    ether = b''
    ether += macpack(dmac)
    ether += macpack(smac)
    ether += ethertype
    '''
    ether_format = [{'name':'smac','type':'mac','value':smac},
    {'name':'dmac','type':'mac', 'value':dmac},
    {'name': 'ethertype', 'type':'raw', 'length':2, 'value':ethertype}]
    ether = (Packer().format(ether_format).to_bytes())
    return ether