# import click

from socket import AF_INET, SOCK_DGRAM, socket, timeout
from struct import unpack, pack
from threading import Thread
from zipfile import ZipFile

import io
import os
from piman import logger

from serializer import Packer
from serializer import Unpacker
"""
This code is modified following Prof. Reed suggestion
"""

"""
The TFTPServer class encapsulates the methods required for running a simple TFTP server that handles only read requests
The server is initialized with a data directory, a port, as well as a connection address

Data directory, port and connection address is specified in the configuration file
(note: sudo must be used if using port 69)
"""

class TFTPServer:
    RRQ_OPCODE = 1
    DATA_OPCODE = 3
    ACK_OPCODE = 4
    ERROR_OPCODE = 5
    OACK_OPCODE = 6
    # TFTP data packets consist of a 2-byte opcode, 2-byte block number, and up to 512-byte data portion
    # Although we could minimize since our server is solely getting RRQ and Ack packets we could have set
    # the buffer to a more optimized value (i.e. filenames on Mac OSX can have up to 256 characters so we
    # could limit the buffer to the max size of a RRQ packet) but for better practice it's been set to the
    # max data packet length in TFTP
    BUFFER_SIZE = 516

    # ctor for setting configurable attributes
    def __init__(self, data_dir, tftp_port, connection_address):
        self.data_dir = data_dir
        self.tftp_port = tftp_port
        self.connection_address = connection_address

    # opens install/boot in zipfile
    def res_open(self, name):
        zipfile = os.path.dirname(os.path.dirname(__file__))
        fd = None
        try:
            with ZipFile(zipfile) as z:
                fd = z.open("install/boot/" + name)
        except KeyError:
            logger.error("{}: key error - looking in filesystem next".format(name))
            pass  # we'll try looking in the filesystem next
        if not fd:
            fd = open("{}/{}".format(self.data_dir, name), "rb")
        if 'cmdline.txt' in name and fd:
            # we need to fixup the master address
            content = fd.read()
            fd.close()
            fd = io.BytesIO(content.replace(b'MASTER', self.connection_address.encode()))
        return fd

    """
    Begins running the server thread
    """

    def start(self):
        self.server_socket = socket(AF_INET, SOCK_DGRAM)
        # We can specify a specific address when running the server (defaults to '')
        logger.info("connecting to {}:{}".format(self.connection_address, self.tftp_port))
        self.server_socket.bind((self.connection_address, self.tftp_port))
        logger.info("serving files from {} on port {}".format(self.data_dir, self.tftp_port))
        self.tftp_thread = Thread(target=self.__process_requests, name="tftpd")
        self.tftp_thread.start()

    def stop(self):
        self.server_socket.close()

    """
    This code is responsible for handling requests (both valid and invalid) as well as ensuring data is transferred
    properly and reliably.
    """

    def __process_requests(self):
        # this while loop keeps our server running also accounting for ensuring the initial
        # data packet is retrieved by the host
        # accepts RRQ's for files and starts a thread to proccess it
        logger.info("TFTP waiting for request")
        while True:

            pkt, addr = self.server_socket.recvfrom(self.BUFFER_SIZE)

            t1 = Thread(
                target=self.__create_thread_and_process_requests, args=(pkt, addr))
            t1.daemon = True
            t1.start()
    """
    This code is responsible for handling requests. It starts a new socket with an ephemeral port
    for communication to the client. If no response is heard after 10 seconds, the socket is closed and function ends.
    """

    def __create_thread_and_process_requests(self, pkt, addr):

        # initial block number and variable for filename
        block_number = 0
        filename = ''
        block_size = 512
        client_dedicated_sock = _create_socket_connection()

        #Handling the first Read Request (RRQ)
        # RRQ is a series of strings, the first two being the filename
        # and mode but there may also be options. see RFC 2347.
        #
        # we skip the first 2 bytes (the opcode) and split on b'\0'
        # since the strings are null terminated.
        #
        # because b'\0' is at the end of all strings split will always
        # give us an extra empty string at the end, so skip it with [:-1]


        strings_in_RRQ = string.decode() for string in pkt[2:].split(b"\0")[:-1]
        logger.info("got {} from {}".format(strings_in_RRQ, addr))
        filename = strings_in_RRQ[0]
        try:
            transfer_file = self.res_open(filename)
        except FileNotFoundError:
            # send an error packet to the requesting host
            logger.error("No such file within the directory")
            packet = self._create_error_packet(17, "No such file within the directory")
            client_dedicated_sock.sendto(packet, addr)
            client_dedicated_sock.close()
            return

        # If RRQ did specify options, send back an option acknowledgement OACK
        if len(strings_in_RRQ) > 4:
            for index, string in enumerate(strings_in_RRQ[2:]):
                if string == 'tsize':
                    #Specifying the size of the transfer file
                    transfer_file.seek(0,2)
                    t_size = transfer_file.tell()
                    transfer_file.seek(0,0)
                if string == 'blksize':
                    block_size = int(strings_in_RRQ[index + 1])

            packet = self._create_oack_packet(t_size, block_size)
            client_dedicated_sock.sendto(packet, addr)

        # Read and send the first chunk of the file
        data = transfer_file.read(block_size)
        if data:
            block_number += 1
            packet = self._create_data_packet(block_number, data)
            client_dedicated_sock.sendto(packet, addr)

        #Handling subsequent acks
        #Wait for ack, send the next chunk, repeat until EOF
        while True:
            try:
                pkt, addr = client_dedicated_sock.recvfrom(self.BUFFER_SIZE)
            except:
                logger.error("Socket Timed Out")
                logger.error('closed socket')
                break
            # the first two bytes of all TFTP packets is the opcode, so we can
            # extract that here. the '!' is for big endian, and 'H' is to say it is an integer
            [opcode] = unpack("!H", pkt[0:2])
            # ACK received, so we can now read the next block, if it doesn't match resend the previous block of data
        
            if opcode == TFTPServer.ACK_OPCODE:
                [acked_block] = unpack("!H", pkt[2:4])
                # block number matches, the block sent was successfully received
                # read next block, increment block number
                if acked_block == block_number:
                    #If the data from last iteration is blank, that means this ack is for the EOF packet, meaning the download is complete
                    if data == b'':
                        logger.warning('download complete, closing socket')
                        break

                    block_number += 1
                    data = transfer_file.read(block_size)

                packet = self._create_data_packet(block_number, data)
                client_dedicated_sock.sendto(packet, addr)
                

            else: #invalid opcode
                # form an error packet and send it to the invalid TID
                logger.error("illegal operation specified")
                packet = self._create_error_packet(20, "illegal operation specified")
                client_dedicated_sock.sendto(packet, addr)
        #End while

        client_dedicated_sock.close()
        transfer_file.close()

    def _create_socket_connection(self):
        # prepare the UDP socket
        client_dedicated_sock = socket(AF_INET, SOCK_DGRAM)
        # bind to 0 for an ephemeral port
        client_dedicated_sock.bind((self.connection_address, 0))
        # set timeout for the socket
        client_dedicated_sock.settimeout(10)
        return client_dedicated_sock

    def _create_oack_packet(self, t_size, block_size):
        '''
        transfer_ack_opcode = pack("!H", TFTPServer.OACK_OPCODE)
        oack_data = 'tsize'.encode() + b'\0' + pack("!I", t_size) + b'\0'
        oack_data += 'block_size'.encode() + b'\0' + pack("!H", block_size) + b'\0'
        packet = transfer_ack_opcode + oack_data
        '''
        oack_format = [{'name':'transfer_ack_opcode','type':'int','length':2, 'value':TFTPServer.OACK_OPCODE},
        {'name':'tsize','type':'cstring', 'value': 'tsize'},
        {'name':'rawbyte','type':'raw', 'value': b'\0'},
        {'name':'t_size','type':'int', 'length': 4, 'value': t_size},
        {'name':'rawbyte','type':'raw', 'value': b'\0'},
        {'name':'block_size','type':'raw','value': 'block_size'},
        {'name':'rawbyte','type':'raw', 'value': b'\0'},
        {'name':'t_size','type':'int', 'length': 4, 'value': block_size},
        {'name':'rawbyte','type':'raw', 'value': b'\0'}
        ]
        packet = (Packer().format(oack_format).to_bytes())
        return packet

    def _create_data_packet(self, block_number, data):
        '''
        transfer_opcode = pack("!H", TFTPServer.DATA_OPCODE)
        transfer_block_number = pack("!H", block_number)
        packet = transfer_opcode + transfer_block_number + data
        '''
        data_format = [{'name':'transfer_opcode','type':'int','length':2, 'value':TFTPServer.DATA_OPCODE},
        {'name':'block_number','type':'int','length':2, 'value': block_number}]
        packet = (Packer().format(data_format).to_bytes())
        return packet

    def _create_error_packet(self, err_code, err_msg):
        '''
        error_opcode = pack("!H", TFTPServer.ERROR_OPCODE)
        error_code = pack("!H", err_code)
        error_message = err_msg.encode() + '\0'
        packet = error_opcode + error_code + error_message
        '''
        error_format = [{'name':'error_opcode','type':'int','length':2, 'value':TFTPServer.ERROR_OPCODE},
        {'name':'destination_port','type':'int','length':2, 'value': err_code},
        {'name': 'error_message','type':'cstring','length':2, 'value': err_msg}]
        packet = (Packer().format(error_format).to_bytes())
        return packet

    def join(self):
        self.tftp_thread.join()


# serializer.packer()
#     .format([
#         {'name': 'opcode', 'type': 'short_int'},
#         {'name': 'err_code', 'type': 'short_int'},
#         {'name': 'msg', 'type': 'cstring'},
#     ])
#     .feed('opcode', TFTPServer.ERROR_OPCODE)
#     .feed('err_code', err_code)
#     .feed('msg', err_msg)
#     .to_bytes()
    



def do_tftpd(data_dir, connection_address, tftp_port):
    """ this is a simple TFTP server that will listen on the specified
        port and serve data rooted at the specified data. only read
        requests are supported for security reasons.
    """
    logger.warning("Starting TFTP...")
    srvr = TFTPServer(data_dir, tftp_port, connection_address)
    srvr.start()
    srvr.join()
    logger.warning("TFTP is terminating")


if __name__ == "__main__":
    do_tftpd()
